<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function index()
	{
        $this->data['services'] = [
            [
                'title' => 'Auto Repairs and Maintenance',
                'description' => 'Motomi has an after-market repairs and maintenance service that fixed a wide range of cars ...',
                'image' => 'auto_repair.jpg',
                'icon' => 'repair',
            ],
            [
                'title' => 'Diesel and Power Generator Repairs',
                'description' => 'Motomi Diesel repairs and maintains a wide range of diesel engines for light weight trucks, construction ...',
                'image' => 'power_gen.jpg',
                'icon' => 'hydrant',
            ],
            [
                'title' => 'Engine Rebuild',
                'description' => '<br /><br />',
                'image' => 'engine_rebuild.jpg',
                'icon' => 'mechanize',
            ],
        ];
    }
}
