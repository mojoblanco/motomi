<section class="page-title  v3 clearfix parallax  parallax5">
    <div class="overlay"></div>
    <div class="container">
        <div class="wrap-page-title">>
            <div class="page-title-heading text-center v2">
                <h1><a href="#" class="hover-text">CAREERS/TRAINING</a></h1>
            </div> <!-- /.page-title-heading -->
        </div> <!-- /.wrap-page-title -->
    </div> <!-- /.container -->
</section> <!-- /.page-title -->

<div class="content-wrap our-team-page ">
    <section class="flat-auto-motive our-team-page">
        <div class="overlay"></div>
        <div class="container ">
            <div class="flat-spacer clearfix" data-desktop="102" data-mobile="60" data-smobile="60" ></div>
            <div class="auto-motive-wrap clearfix">
                <div class="img one-of-two">
                    <img src="/assets/images/training.jpg" alt="Training">
                </div>
                <div class="content one-of-two">
                    <div class="flat-spacer clearfix" data-desktop="48" data-mobile="20" data-smobile="20" ></div>
                    <div class="title">
                        <a href="#">
                            <h3>TRAINING</h3>
                        </a>
                    </div>
                    <p>
                        We offer training packages for aspiring men and women who wish to be trained technicians in Automotive repairs, Diesel power generation and engine remanufacturing program
                    </p>
                    <p>
                        The training lasts for a duration of TWO (2) years.
                    </p>
                    <p>
                        If you are interested in the training or would like to work with us, kindly send an email with a copy of your CV to motomirepairs@gmail.com or click the button below.
                    </p>
                    <div class="wrap-btn">
                        <a href="mailto:motomirepairs@gmail.com" class="flat-button bg-read-more v2">Get in touch</a>
                    </div>
                </div>
            </div>
            <div class="flat-spacer clearfix" data-desktop="70" data-mobile="70" data-smobile="70" ></div>
        </div> <!-- /.container -->
    </section> <!-- /.flat-auto-motive -->
</div>