<section class="flat-warranty">
    <div class="container">
        <div class="flat-carousel-not-numb" data-auto="false" data-column="3" data-column2="2" data-column3="1" data-gap="30" data-dots="true" data-nav="false" data-loop="true" >
            <div class="flat-fullbox style1 clearfix  owl-carousel">

                <?php foreach($services as $service): ?>
                    <div class="fullbox style1  text-center">
                        <div class="fullbox-img">
                            <div class="fullbox-img">
                                <img src="/assets/images/<?= $service['image'] ?>" alt="<?= $service['image'] ?>">
                                <div class="overlay"></div>
                            </div>
                            <div class="fullbox-icon">
                                <span class="icon-<?= $service['icon'] ?>"></span>
                            </div>
                            <div class="fullbox-content">
                                <h4><a href="#"><?= $service['title']; ?></a></h4>
                                <p><?= $service['description'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div> <!-- /.flat-fullbox -->
        </div>

    </div> <!-- /.container -->
</section> <!-- /.flat-warranty -->