<section class="page-title  v3 clearfix parallax  parallax5">
    <div class="overlay"></div>
    <div class="container">
        <div class="wrap-page-title">>
            <div class="page-title-heading text-center v2">
                <h1><a href="#" class="hover-text">AUTO REPAIRS AND MAINTENANCE</a></h1>
            </div> <!-- /.page-title-heading -->
        </div> <!-- /.wrap-page-title -->
    </div> <!-- /.container -->
</section> <!-- /.page-title -->

<article class="content-wrap">
    <div class="flat-spacer clearfix" data-desktop="99" data-mobile="99" data-smobile="99" ></div>
    <div class="container clearfix">

        <?php $this->load->view('shared/service_sidebar'); ?>

        <div class="content-page-wrap about-company-wrap pd-left-60">
            <div class="flat-spacer clearfix" data-desktop="0" data-mobile="60" data-smobile="60" ></div>
            <div class="flat-single-service pd-bottom-50 mg-bottom-55">
                <h2 class="title">Auto Repairs and Maintenance</h2>
                <p>
                    Motomi has an after-market repairs and maintenance service that fixed a wide range of cars, from the Asian Toyota, Honda, to German Mercedes and Audi and American Ford, GMC. 
                </p>
                <p>
                    Motomi Fleet makes managing your fleet vehicles easier. With more than 7 years of experience in fleet maintenance, our use of state-of-the-art equipment by professional technicians, and much more, Motomi is the one-stop shop for fleet managers expecting quality experience and value every time, for every vehicle
                </p>

                <div class="flat-featured-services">
                    <div class="flat-title v4">
                        <h3>Our featured services</h3>
                    </div>

                    <p>
                        We partner with insurance companies to bring back accident cars to life.
                    </p>
                </div>

                <div class="flat-featured-services">
                    <div class="flat-title v4">
                        <h3>Repair and Maintenance</h3>
                    </div>

                    <p>
                        We depend on our vehicles to get us where we need to go, when we need to go somewhere. That’s why it’s so frustrating when our vehicle breaks down on us, whether something’s just worn out over time or suddenly breaks. We strive to provide our customers with comprehensive information as to why a repair is needed, how the auto part has failed, and what measures can be taken to care for the replacement part and avoid failure in the future.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="flat-spacer clearfix" data-desktop="80" data-mobile="80" data-smobile="80" ></div>
</article>
