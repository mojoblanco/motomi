<section class="page-title  v3 clearfix parallax  parallax5">
    <div class="overlay"></div>
    <div class="container">
        <div class="wrap-page-title">>
            <div class="page-title-heading text-center v2">
                <h1><a href="#">DIESEL AND POWER GENERATOR REPAIRS</a></h1>
            </div> <!-- /.page-title-heading -->
        </div> <!-- /.wrap-page-title -->
    </div> <!-- /.container -->
</section> <!-- /.page-title -->

<article class="content-wrap">
    <div class="flat-spacer clearfix" data-desktop="99" data-mobile="99" data-smobile="99" ></div>
    <div class="container clearfix">

        <?php $this->load->view('shared/service_sidebar'); ?>

        <div class="content-page-wrap about-company-wrap pd-left-60">
            <div class="flat-spacer clearfix" data-desktop="0" data-mobile="60" data-smobile="60" ></div>

            <div class="flat-single-service pd-bottom-50">
                <h2 class="title">Diesel and Power Generator Repairs</h2>
                <p>
                    Motomi Diesel repairs and maintains a wide range of diesel engines for light weight trucks, construction, refrigeration, airline ground support, agriculture and power generation. Our experience and training has set us aside. Our team is trained in CAT, Cummins, International, Perkins and installation of gas powered generators.
                </p>
            </div>

            <div class="flat-support ">
                <div class="flat-title v4">
                    <h3 class="title">Our process for Motomi Diesel</h3>
                </div>
                <div class="accordion">
                    <?php foreach($process as $i => $item): ?>
                        <div class="accordion-toggle">
                            <div class="toggle-title <?= $i == 0 ? 'active' : ''; ?>">
                                <?= $item['title']; ?>
                            </div>
                            <div class="toggle-content" >
                                <p><?= $item['text']; ?></p>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="flat-spacer clearfix" data-desktop="80" data-mobile="80" data-smobile="80" ></div>

</article>
