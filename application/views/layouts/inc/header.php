<header id="header" class="header-v3">
    <div class="flat-spacer clearfix" data-desktop="40" data-mobile="40" data-smobile="40"   ></div>
    <div class="container clearfix">
        <div id="logo">
            <a href="/">
                <img src="/assets/images/motomi.png" alt="logo" width="175" height="54" data-retina="/assets/images/motomi.png" data-width="175" data-height="54">
            </a>
        </div> <!-- /#logo -->

        <div class="mobile-button v2">
            <span></span>
        </div>

        <div class="nav-wrap text-right">
            <nav id="mainnav" class="mainnav header-v3">
                <ul class="menu">
                    <li>
                        <a href="/">HOME</a>
                    </li>
                    <li>
                        <a href="#">SERVICES</a>
                        <ul class="submenu">
                            <li><a href="/services/autorepairs"> AUTO REPAIRS AND MAINTENANCE</a></li>
                            <li><a href="/services/genrepairs"> DIESEL AND POWER GENERATOR REPAIRS</a></li>
                            <li><a href="#"> ENGINE REBUILD</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/careers">CAREERS</a>
                    </li>
                </ul>
            </nav><!-- /.mainnav -->
        </div>
    </div> <!-- /.container -->
    <div class="flat-spacer clearfix" data-desktop="36" data-mobile="36" data-smobile="36" ></div>
</header> <!-- /#header -->