<footer id="footer" class="style1">
    <div class="container">
        <div class="flat-spacer clearfix"></div>

        <div id="logo-ft" class=" text-center ">
            <a href="index.html"><img src="/assets/images/motomi.png" alt="AutoMov" width="175" height="158" data-retina="/assets/images/motomi.png" data-width="175" data-height="158"></a>
        </div> <!-- #logo-ft -->

        <div class="flat-spacer clearfix" data-desktop="34" data-mobile="34" data-smobile="34" ></div>

        <div class="flat-socails v2 text-center">
            <ul>
                <li>
                    <a href="https://www.facebook.com/Motominigeria/">
                        <span class="social_facebook"></span>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/motomirepairs">
                        <span class="social_twitter"></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/motomirepairs">
                        <span class="social_instagram"></span>
                    </a>
                </li>
            </ul>
        </div> <!-- /.flat-socails -->
        <div class="flat-spacer clearfix" data-desktop="38" data-mobile="38" data-smobile="38" ></div>

        <div class="flat-copy-right text-center">
            <p>&copy; <?= date('Y') ?>. All Rights Reserved.</p>
        </div> <!-- /.flat-copy-right -->
    </div> <!-- /.container -->
</footer> <!-- /#footer -->