<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>Motomi Auto Repair and Maintenance</title>

        <meta name="author" content="themsflat.com">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="/assets/stylesheet/style.css">
        <link rel="stylesheet" type="text/css" href="/assets/stylesheet/colors/color1.css" id="colors">

        <!-- Responsive -->
        <link rel="stylesheet" type="text/css" href="/assets/stylesheet/responsive.css">

        <link rel="stylesheet" type="text/css" href="/assets/stylesheet/custom.css">

    </head>
    <body>
        <?php $this->load->view('layouts/inc/header'); ?>

        <?= $yield ?>

        <?php $this->load->view('layouts/inc/footer'); ?>

        <!-- Javascript -->
        <script src="/assets/javascript/jquery.min.js"></script>
        <script src="/assets/javascript/plugins.js"></script>
        <script src="/assets/javascript/owl.carousel.min.js"></script>
        <script src="/assets/javascript/imagesloaded.min.js"></script>
        <script src='/assets/javascript/jquery-isotope.js'></script>
        <script src="/assets/javascript/equalize.min.js"></script>

        <script src="/assets/javascript/main.js"></script>

    </body>
</html>