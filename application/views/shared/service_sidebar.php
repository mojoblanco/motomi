<div class="sidebar-left">
    <div class="widget widget-categories v1">
        <ul>
            <li><a href="/services/autorepairs">Auto Repairs and Maintenance</a></li>
            <li><a href="/services/genrepairs">Diesel and Power Generator Repairs</a></li>
            <li><a href="/services/autorepairs">Engine Rebuild</a></li>
    </div>

    <div class="widget widget-deal">
        <div class="textbox">
            <h3 class="title">MAKE AN APPOINTMENT</h3>
            <p>
                Give us a call today to schedule your appointment and we will make sure everything is ready to get your car back on the road. <br>
                <strong>Call: 08033603906</strong>
            </p>

            <hr>

            <h3 class="title">REQUEST AN ESTIMATE</h3>
            <p>
                Get started with your auto body repair by giving us a call or sending us Watsapp pictures and answering some questions. <br>
                <strong>Call: 08033603906</strong>
            </p>
        </div>
    </div>
</div>